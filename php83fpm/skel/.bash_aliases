# Some more bash aliases.

alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias ll='ls -alhF --color=tty'
alias ls='ls -F --color=tty'
