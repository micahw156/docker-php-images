#!/bin/bash

# Log in to registry to make sure push will work.

docker login

# Set up buildx environment first:
#
# docker buildx create --name mybuilder --driver docker-container --bootstrap --use
# docker buildx ls
# docker buildx inspect

docker buildx use mybuilder

for I in 7.4 8.1 8.2 8.3
do
  D=$(echo ${I} | sed 's/\.//')
  (echo php${D}fpm;    cd php${D}fpm ;    docker buildx build --platform linux/amd64,linux/arm64 -t micahw156/php:${I}-fpm --push .;    echo)
  (echo php${D}drupal; cd php${D}drupal ; docker buildx build --platform linux/amd64,linux/arm64 -t micahw156/php:${I}-drupal --push .; echo)
done

docker pull micahw156/php -a
docker image prune -f
docker image ls micahw156/php
