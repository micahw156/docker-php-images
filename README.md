# Docker image build files for PHP

This repository contains Docker image files used for PHP development and site building.

These images are published at https://hub.docker.com/repository/docker/micahw156/php

These images are built on the latest [php-fpm](https://hub.docker.com/_/php) images at build time, adding custom packages and software to suit the current needs of the Henry Ford College Web Team.

PHP versions 7.4, 8.1, 8.2, and 8.3 are supported.

The **php:x.x-fpm** labels have proven suitable for working with Symfony projects and hand-coded PHP.

The **php:x.x-drupal** labels add desirable bash aliases in the user profile. Drush for PHP 7.4 is locked at 8.3.5 release for stability. Higher PHP versions add /var/www/html/vendor/bin to the path instead of providing Drush.

The **build-all-images.sh** script generates multi-platform images for linux/amd64 and linux/arm64 platforms, making these images suitable for use in Linux, WSL2, and Intel or Apple Silicon MacOS environments.
