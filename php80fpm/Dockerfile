FROM php:8.0-fpm

RUN ln -sf /usr/share/zoneinfo/America/Detroit /etc/localtime

RUN apt update \
    && apt install -y g++ ca-certificates curl gnupg git mariadb-client rsync vim zip \
        libfreetype6-dev libicu-dev libjpeg-dev libldap2-dev libpng-dev \
        libpq-dev libwebp-dev libzip-dev zlib1g-dev  \
    && pecl install apcu \
    && docker-php-ext-configure gd --with-freetype --with-jpeg=/usr --with-webp \
    && docker-php-ext-configure ldap \
    && docker-php-ext-configure zip \
    && docker-php-ext-enable apcu \
    && docker-php-ext-install bcmath gd intl ldap mysqli opcache pdo pdo_mysql zip

WORKDIR /var/www/html

COPY docker-php-zzz-local.ini "$PHP_INI_DIR/conf.d/docker-php-zzz-local.ini"
COPY ldap.conf /etc/ldap/ldap.conf

COPY skel /etc/skel
RUN mkdir /etc/skel/.ssh && chmod 700 /etc/skel/.ssh

RUN curl -fsSL https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
    && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" \
    | tee /etc/apt/sources.list.d/nodesource.list \
    && apt-get update \
    && apt-get install nodejs -y \
    && npm install -g npm@10 \
    && npm install -g --save-dev stylelint stylelint-config-standard stylelint-order \
    && npm install -g --save-dev --save-exact prettier

RUN groupadd -g 404 webmxr && useradd -g 404 -u 404 -s /bin/bash -m -c 'Webmaster' webmxr

USER webmxr:webmxr

COPY bin /usr/local/bin
COPY home /home/webmxr
